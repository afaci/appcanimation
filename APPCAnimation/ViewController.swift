//
//  ViewController.swift
//  APPCAnimation
//
//  Created by Manuel Baumgartner on 18/05/2015.
//  Copyright (c) 2015 Application Project Center. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var image : AnimatedImage?
    @IBOutlet weak var imView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        image = AnimatedImage.animatedImageWithName("rocket.gif")
        imView.setAnimatedImage(image!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func playPressed(sender: AnyObject) {
        imView.stopAnimatingGIF()
        image?.setDuration(0.04)
        imView.startAnimatingGIF()
    }

    @IBAction func pausePressed(sender: AnyObject) {
        imView.stopAnimatingGIF()
    }
    
    @IBAction func fastForwardPressed(sender: AnyObject) {
        imView.stopAnimatingGIF()
        image?.setDuration(0.01)
        imView.startAnimatingGIF()
    }
}

